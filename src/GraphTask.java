import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */

/*
Say that an n-vertex directed acyclic graph G is compact if there is some way
of numbering the vertices of G with the integers from 0 to n − 1 such that G
contains the edge (i, j) if and only if i < j, for all i, j in [0, n − 1]. Give an O(n2)
time algorithm for detecting if G is compact.
 */

public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
//      //Pattern1 : There is a vertex reversing from bottom to top[False]
//      Vertex a1 = new Vertex("1v");
//      Vertex b1 = new Vertex("2v");
//      Vertex c1 = new Vertex("3v");
//      Vertex d1 = new Vertex("4v");
//      a1.next = b1;
//      b1.next = c1;
//      c1.next = d1;
//      Graph g1 = new Graph ("G1", a1);
//      g1.createArc("arc1",a1, b1);
//      g1.createArc("arc4",b1, c1);
//      g1.createArc("arc5",b1, d1);
//      g1.createArc("arc6",d1, a1);
//      System.out.println(g1);
//      int[][] arr1 = g1.createAdjMatrix();
//      int i=0;
//      for (int[] ints : arr1) {
//         System.out.println(Arrays.toString(ints));
//         i++;
//      }
//      if(g1.isCompact()){
//         System.out.println("G1 is compact");
//      }else{
//         System.out.println("G1 is not compact");
//      }
//
//      //Pattern2 : There is a vertex never used[False]
//      Vertex a2 = new Vertex("1v");
//      Vertex b2 = new Vertex("2v");
//      Vertex c2 = new Vertex("3v");
//      Vertex d2 = new Vertex("4v");
//      a2.next = b2;
//      b2.next = c2;
//      c2.next = d2;
//      Graph g2 = new Graph ("G2", a2);
//      g2.createArc("arc1",a2,b2);
//      g2.createArc("arc2",b2,c2);
//      System.out.println(g2);
//      int[][] arr2 = g2.createAdjMatrix();
//      i=0;
//      for (int[] ints : arr2) {
//         System.out.println(Arrays.toString(ints));
//         i++;
//      }
//      if(g2.isCompact()){
//         System.out.println("G2 is compact");
//      }else{
//         System.out.println("G2 is not compact");
//      }
//
//      //Pattern3 : There is only one vertex in the graph[False]
//      Vertex a3 = new Vertex("1v");
//      Graph g3 = new Graph("G3",a3);
//      System.out.println(g3);
//      int[][] arr3 = g3.createAdjMatrix();
//      i=0;
//      for (int[] ints : arr3) {
//         System.out.println(Arrays.toString(ints));
//         i++;
//      }
//      if(g3.isCompact()){
//         System.out.println("G3 is compact");
//      }else{
//         System.out.println("G3 is not compact");
//      }
//
//      //Pattern4 : Simple DAG[True]
//      Vertex a4 = new Vertex("1v");
//      Vertex b4 = new Vertex("2v");
//      Vertex c4 = new Vertex("3v");
//      Vertex d4 = new Vertex("4v");
//      a4.next = b4;
//      b4.next = c4;
//      c4.next = d4;
//      Graph g4 = new Graph ("G4", a4);
//      g4.createArc("arc1",a4, b4);
//      g4.createArc("arc4",b4, c4);
//      g4.createArc("arc5",b4, d4);
//      System.out.println(g4);
//      int[][] arr4 = g4.createAdjMatrix();
//      i=0;
//      for (int[] ints : arr4) {
//         System.out.println(Arrays.toString(ints));
//         i++;
//      }
//      if(g4.isCompact()){
//         System.out.println("G4 is compact");
//      }else{
//         System.out.println("G4 is not compact");
//      }
//
//      //Pattern5 : Crossed DAG[True]
//      Vertex a5 = new Vertex("1v");
//      Vertex b5 = new Vertex("2v");
//      Vertex c5 = new Vertex("3v");
//      Vertex d5 = new Vertex("4v");
//      a5.next = b5;
//      b5.next = c5;
//      c5.next = d5;
//      Graph g5 = new Graph ("G5", a5);
//      g5.createArc("arc1",a5, d5);
//      g5.createArc("arc4",b5, c5);
//      g5.createArc("arc5",c5, d5);
//      System.out.println(g5);
//      int[][] arr5 = g5.createAdjMatrix();
//      i=0;
//      for (int[] ints : arr5) {
//         System.out.println(Arrays.toString(ints));
//         i++;
//      }
//      if(g5.isCompact()){
//         System.out.println("G5 is compact");
//      }else{
//         System.out.println("G5 is not compact");
//      }

      //Pattern6 : 2000 Vertices
      long startTime = System.nanoTime();
      Graph randG = new Graph("g");
      randG.createRandomSimpleGraph(2000,1999);
      int[][] arr6 = randG.createAdjMatrix();
      int i=0;
      for (int[] ints : arr6) {
         System.out.println(Arrays.toString(ints));
         i++;
      }
      if(randG.isCompact()){
         System.out.println("G5 is compact");
      }else{
         System.out.println("G5 is not compact");
      }
      long endTime = System.nanoTime();

      long timeElapsed = endTime - startTime;
      System.out.println("Execution time in nanoseconds: " + timeElapsed);
      System.out.println("Execution time in milliseconds: " + timeElapsed / 1000000);

      // TODO!!! Your experiments here
   }

   // TODO!!! add javadoc relevant to your problem
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                       + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                       + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.
      /*
      Determine if the graph is compact or not.
      If the vertices of the graph with the integers from 0 to n − 1 such that the graph
      contains the edge (i, j) if and only if i < j, for all i, j in [0, n − 1].
      if the graph is compact, return TRUE
      if the graph is not compact, return FALSE and print where it's wrong with '*'
      INPUT: Graph
      OUTPUT: True or False
       */
      public boolean isCompact(){
         boolean compact = true;
         int[][] Matrix = this.createAdjMatrix();
         int [] vertices = new int[Matrix.length];

         //Only one vertex
         if(Matrix.length < 2){
            System.out.println("Only one vertex:");
            return false;
         }

         //Check the flow of graph is collect
         //Double for loop => O(n^2) times
         for(int i=0; i<Matrix.length; i++){
            for(int j=0; j<Matrix.length; j++){
               //Check connected vertices
               if(Matrix[i][j]>0){
                  vertices[i]=1;
                  vertices[j]=1;
               }

               //Detect loop
               if(Matrix[i][j] > 0 && i == j){
                  System.out.println("Vertex has a loop: Vertex");
                  this.printMatrix(i,j);
                  compact = false;
               }

               //Detect reversing
               if(Matrix[i][j] > 0 && i > j){
                  System.out.println("[Back Edge]Vertex is reversing: i > j");
                  this.printMatrix(i,j);
                  compact = false;
               }
            }
         }

         //Check if all the vertex is ever used
         int i=1;
         for (int vertex : vertices) {
            if(vertex == 0){
               System.out.println("There is a vertex which has never used : vertex"+i);
               compact = false;
            }
            i++;
         }
         return compact;
      }

      /*
      Printout Matrix and show where it's wrong
      i, j => location that does not meet condition
      */
      public void printMatrix(int i, int j){
         System.out.println("+------MATRIX-------+");
         int[][] matrix = this.createAdjMatrix();

         System.out.print("  ");
         for(int x=0;x<matrix.length;x++){
            System.out.print(x+1+" ");
         }
         System.out.print("\n  ");
         for(int x=0;x<matrix.length;x++){
            System.out.print("_ ");
         }
         System.out.print("\n");
         int x=0; int y=0;
         for(x=0; x< matrix.length; x++){
            System.out.print(x+1+"|");
            for(y=0; y< matrix.length; y++){
               if(x == i && y == j){
//                  System.out.print("\u001b[31m");
                  System.out.print(matrix[x][y]+"*");
//                  System.out.print("\u001b[00m");
               }else{
                  System.out.print(matrix[x][y]+" ");
               }
            }
            System.out.print("\n");
         }
         System.out.println("+------MATRIX-------+");
      }
   }

} 

